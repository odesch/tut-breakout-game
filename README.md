# README #

This is a basic 2D breakout game from LUKEZ's tutorial on Youtube [[here](https://www.youtube.com/playlist?list=PLhcBwf4tKMouKEApgJH4BiDgg8zKwNQQw)].

* Unity3D Version 5.2
* `firstLevel` in Scenes folder is the starting point and the only scene in the project.
+ Sound referances,
    * Paddle Sound: [[here](https://www.freesound.org/people/fins/sounds/146721/)]. Credit:  freesound.org - fins 
    * Wall Sound: [[here](https://www.freesound.org/people/oceanictrancer/sounds/233539/)]. Credit: freesound.org - oceanictrancer
    * Block Sound: [[here](https://www.freesound.org/people/Eelke/sounds/170424/)]. Credit: freesound.org - Eelke