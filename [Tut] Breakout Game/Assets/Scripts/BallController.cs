﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

    PaddleController paddleController;
    GameObject paddle, theBall;

    public GameObject theBallPrefab;
    public AudioClip blockSound, wallSound;
    
    // Use this for initialization
    void Start () {
        paddle = GameObject.FindGameObjectWithTag("Paddle");
        paddleController = paddle.GetComponent<PaddleController>();

	}
	
	// Update is called once per frame
	void Update () {
        if(transform.position.y < -5)
        {
            //TODO: Decrease the lives.

            theBall = Instantiate(theBallPrefab, paddle.transform.position + new Vector3(0, 0.3f, 0), Quaternion.identity) as GameObject;
            paddleController.attachedBall = theBall;
            Destroy(gameObject);
        }
	
	}

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Wall")
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(wallSound);
        }

        if(col.gameObject.tag == "Block")
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(blockSound);
            Destroy(col.gameObject);
        }
    }
}
