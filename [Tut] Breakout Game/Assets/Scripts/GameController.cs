﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{

    public GameObject block;

    // Use this for initialization
    void Start()
    {

        BlockCreate();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void BlockCreate()
    {
        for (int h = -3; h < 4; h++)
        {
            for (int v = 1; v < 5; v++)
            {
                Instantiate(block, new Vector3(h * 1.1f, v, 0), Quaternion.identity);
            }

        }
    }
}
