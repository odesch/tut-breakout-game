﻿using UnityEngine;
using System.Collections;

public class PaddleController : MonoBehaviour
{

    float xBoundary = 0, maxBoundary = 5.6f;

    public float yPosition = -3.5f, zPosition = 0;
    public float paddleSpeed = 20, ballSpeed = 500;

    public AudioClip paddleSound;

    public GameObject ballPrefab;
    public GameObject attachedBall = null;

    public Rigidbody ballRigidbody;

    // Use this for initialization
    void Start()
    {
        SpawnBall();
    }

    // Update is called once per frame
    void Update()
    {

        //Paddle Movement
        if (Input.GetAxis("Horizontal") != 0)
        {
            transform.position = new Vector3(
                                transform.position.x + Input.GetAxis("Horizontal") * paddleSpeed * Time.deltaTime,
                                yPosition, zPosition);

            var boundary = maxBoundary - xBoundary;

            #region This way does not work
            //Left Hand Side
            //if (transform.position.x < boundary)
            //{
            //    transform.position = new Vector3(boundary, yPosition, zPosition);
            //}

            //Right Hand Side
            //else if (transform.position.x > -boundary)
            //{
            //    transform.position = new Vector3(-boundary, yPosition, zPosition);
            //}
            #endregion

            //Left Hand Side
            if (transform.position.x < -boundary)
            {
                transform.position = new Vector3(-boundary, yPosition, zPosition);
            }

            //Right Hand Side
            else if (transform.position.x > boundary)
            {
                transform.position = new Vector3(boundary, yPosition, zPosition);
            }

        }

        if (attachedBall)
        {
            ballRigidbody = attachedBall.GetComponent<Rigidbody>();
            float ballSize = attachedBall.GetComponent<Transform>().localScale.y;

            ballRigidbody.position = transform.position + new Vector3(0, ballSize, 0);

            if (Input.GetButtonDown("Jump"))
            {
                ballRigidbody.isKinematic = false;
                ballRigidbody.AddForce(0, ballSpeed, 0);
                attachedBall = null;
            }
        }

        
    }
    void SpawnBall()
    {
        attachedBall = Instantiate(ballPrefab, transform.position + new Vector3(0, .3f, 0), Quaternion.identity) as GameObject;
    }

    void OnCollisionEnter(Collision col)
    {
        gameObject.GetComponent<AudioSource>().PlayOneShot(paddleSound);

        foreach(var contact in col.contacts)
        {
            //if the ball's collider touches the paddle's collider
            if(contact.thisCollider == gameObject.GetComponent<Collider>())
            {
                float ballAngle = contact.point.x - transform.position.x;
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(100 * ballAngle, 0, 0);
            }
        }


    }
}
